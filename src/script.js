/*
Если переменная a равна 10, то выведите 'Верно', иначе выведите 'Неверно'.
2.В переменной min лежит число от 0 до 59. Определите в какую четверть часа попадает это число (в первую, вторую, третью или четвертую).
*/

/*const a = parseInt(prompt('Введите число')); 

if (a===10) {
    document.write('Верно');
}
else if (a !== 10) {
    document.write("Неверно");
};
*/

const min = parseInt(prompt ('Введите число от 0 до 59'));

if (min < 15) {
    document.write('первая четверть');
} else if (min < 30) {
    document.write('Вторая четверть');
} else if (min < 45) {
    document.write('Третья четверть');
} else if (min < 60) {
    document.write('Четвертая четверть');
} else {
    console.error('Ошибка');
};